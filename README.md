# Manual for setting up IntroDH VM 

Given all the different OS and software versions used by students, we have decided to package some tools necessary to run our scripts, libraries, and tools in a virtual machine and ship it in a Vagrant box, the `IntroDH.box`. This keeps OS specific helpdesking and troubleshooting to a minimum. All you have to do to get the `IntroDH.box` is to install VirtualBox and Vagrant and initialise the box. Please find more information below. Due to the lack of a secure password / username, please do not save any sensitive information on your virtual machine. `IntroDH.box` is a learning environment and not a permament solution for virtualising your research. If you would like to build a virtual machine that is similar to `IntroDH.box`, but safer, we are happy to give some hints and advice.

## What is in the `IntroDH.box`?

#### OS

Ubuntu 16.04 LTS, 64bit, 1024 MB RAM (you can alter the RAM later in the vagrantfile)

#### Languages

- Java 1.7 (also 1.8: use `sudo update-alternatives --config java` to switch between versions)
- R, version 3.2.3 “Wooden Christmas Tree” / RStudioServer
- Git, 1.7.9.5

#### Libraries, DBs, Software

- OpenRefine, 2.6.-beta.1

## How can I get the LeipzigDH.box?

You need to install VirtualBox and Vagrant. Then download and initialise `IntroDH.box`

### Download VirtualBox

Find VirtualBox [here](https://www.virtualbox.org).

### Download Vagrant

Find Vagrant [here](https://www.vagrantup.com).

### Download `IntroDH.box`

Download the `IntroDH.box` from [here](https://1drv.ms/u/s!AnvLWEgn0XvVhq8y9YHD2m1KkQHe6g)

### Setting up `IntroDH.box`

You should get familiar with the terminal / command line interface of your operating system. For more information see: [Linux](http://askubuntu.com/questions/38162/what-is-a-terminal-and-how-do-i-open-and-use-it), [Mac](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line), [Windows](http://www.digitalcitizen.life/7-ways-launch-command-prompt-windows-7-windows-8).

1. Create a folder for `IntroDH.box` and move `IntroDH.box` there and switch to it
E.g. 
```
mkdir ~/somefolder/DHLeipzig/vagrant_boxes
cd ~/somefolder/DHLeipzig/vagrant_boxes
```

2. Move `IntroDH.box` to the new folder
3. Add `IntroDH.box` to Vagrant: `vagrant box add IntroDH IntroDH.box`
4. Initialise the IntroDH.box: `vagrant init IntroDH`
5. Start it for the first time:: `vagrant up`
6. Halt the IntroDH.box: `vagrant halt`
7. Modify vagrantfile so it starts with a GUI and activate port-forwarding (lines you have to add). Find and uncomment in the vagrantfile (open with an editor of your choice):
```
 config.vm.network "forwarded_port", guest: 3333, host: 3333 # To use OpenRefine on your host machine
 config.vm.network "forwarded_port", guest: 8787, host: 8787 # To use RStudio on your host machine
 config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024" # Uncomment this and change it to alter the RAM of your VM
end
```

8. Start again: `vagrant up` 
9. Enjoy all the pre-installed tools and libraries
10. If you want to delete your VM, type `vagrant destroy`


